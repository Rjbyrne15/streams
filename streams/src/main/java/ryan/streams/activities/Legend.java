package ryan.streams.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ryan.streams.R;

public class Legend extends AppCompatActivity {

    TextView gauge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legend);
        Button back_button = findViewById(R.id.back_button_legend);
        Button email_button= findViewById(R.id.emailPaStreams_btn);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        email_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","pastreams@gmail.com", null));
                startActivity(Intent.createChooser(emailIntent,"sending email"));

            }
        });



        //gauge.setTextColor(Color.parseColor("Hue_Orange"));

        //        android:textColor="#ff009416"
        //        android:textColor="#ff070085"
        //        android:textColor="#ff7a8500"
        //        android:textColor="#ff8c8f00"
    }

}
