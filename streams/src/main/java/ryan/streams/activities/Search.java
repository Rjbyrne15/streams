package ryan.streams.activities;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ryan.streams.R;
import ryan.streams.objects.Gauge;

public class Search extends ListActivity {

    private List<String> cities_1;
    private ArrayList<HashMap<String, String>> cities;
    private ArrayList<Gauge> gauges;
    private ArrayList<String> list;
    private ArrayAdapter<String> itemsAdapter;
    private Context mContext;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        handleIntent(getIntent());
        Button button = findViewById(R.id.but);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        //ListView v = findViewById(R.id.list);

    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            mContext = getApplicationContext();
            //itemsAdapter.setNotifyOnChange(true);
           doSearch(query);
        }
    }

    private void doSearch(String query){
        list= new ArrayList<>();
        //TextView tv = findViewById(R.id.txv);

        list.add("Coordinate");
        list.add("---------------------------");
        if(isCoordinate(query)){
            list.add(query);
        }


        if((gauges = MapPage.allData.getGauges())!=null) {
            list.add("River Gauges");
            list.add("---------------------------");
            for(Gauge g:gauges) {
                if(g.getName().toLowerCase().contains(query.toLowerCase().trim())) {
                    list.add(g.getName());
                    //MapPage.allData.setSelectedLoc(g.getLatlon());
                    //MapPage.allData.setSelectedName(g.getName());
                    //MapPage.allData.changed = true;
                    //MapPage.allData.which = 0;
                }
            }
        }
        cities = MapPage.allData.getCities(query.trim());
        cities_1 = new ArrayList<>();

        if(!cities.isEmpty()){
            //LatLng loc = new LatLng(Double.parseDouble( cities.get(0).get("lat")),Double.parseDouble(cities.get(0).get("lng")));
            list.add("Cities, Towns");
            list.add("---------------------------");
            for(HashMap<String,String> city: cities){
                Log.e("City is",city.get("city")+", "+city.get("state_id"));
                cities_1.add(city.get("city")+", "+city.get("state_id"));
                list.add(city.get("city")+", "+city.get("state_id"));
            }
            Log.e("lat and lng", cities.get(0).get("lng") + " " + cities.get(0).get("lat")+ " these are them");
            //MapPage.allData.setSelectedLoc(loc);
            //MapPage.allData.changed = true;
            //MapPage.allData.which = 1;
        }
        if(!list.isEmpty()) {
            Log.e("cities_1", "not empty");

            //itemsAdapter.addAll(cities_1);
            //setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item,MOBILE_OS));
            ListAdapter adapter = getListAdapter();
            TextView item= findViewById(R.id.list_items);
            //setListAdapter(new ArrayAdapter<String>(mContext, R.layout.list_item,R.id.list_items ,list));

        }else{
            list.add("No Results");
        }
        setListAdapter(new ArrayAdapter<String>(mContext, R.layout.list_item,R.id.list_items ,list));

    }

    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);

        String selectedItem = (String) getListView().getItemAtPosition(position);
        if(cities_1.contains(selectedItem)){
            for(HashMap<String,String> city: cities){
                if((city.get("city")+", "+city.get("state_id")).equals(selectedItem)){
                    LatLng loc = new LatLng(Double.parseDouble( city.get("lat")),Double.parseDouble(city.get("lng")));
                    MapPage.allData.setSelectedLoc(loc);
                    MapPage.allData.setSelectedName(city.get("city"));
                    MapPage.allData.changed = true;
                    MapPage.allData.which = 1;
                }
            }
            //get city coordinates
        } else if(position == 2) {
            MapPage.allData.setSelectedLoc(coordinateToLatLng(selectedItem));
            MapPage.allData.changed = true;
            MapPage.allData.which = 2;

        }else{
            //get gauge coordinates;
            for(Gauge g:gauges) {
                if(g.getName().toLowerCase().contains(selectedItem.toLowerCase().trim())) {
                    MapPage.allData.setSelectedLoc(g.getLatlon());
                    MapPage.allData.setSelectedName(g.getName());
                    MapPage.allData.changed = true;
                    MapPage.allData.which = 0;
                }
            }


        }

        //String selectedItem = (String) getListAdapter().getItem(position);
        finish();

        Log.e("CLICK","You clicked " + selectedItem + " at position " + position);
    }


    private boolean isCoordinate(String searchQuery){
        if(searchQuery.contains(",") && searchQuery.contains(".")){
            return true;
        }
        return false;
    }


    private LatLng coordinateToLatLng(String s){
        if(s!=null) {
            return new LatLng(Double.parseDouble(s.substring(0, s.indexOf(",")).trim()), Double.parseDouble(s.substring(s.indexOf(",")+1, s.length()).trim()));
        }else {
            return null;
        }
    }

}
