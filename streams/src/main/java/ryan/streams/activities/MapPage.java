package ryan.streams.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.geojson.GeoJsonLayer;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import ryan.streams.R;
import ryan.streams.helper.ConnectionHandler;
import ryan.streams.helper.LayerHandler;
import ryan.streams.helper.PopupAdapter;
import ryan.streams.helper.Searchable_Data;
import ryan.streams.helper.Stream_Gauge_Data;

import static ryan.streams.helper.Searchable_Data.getSelectedLoc;
import static ryan.streams.helper.Searchable_Data.getSelectedName;

public class MapPage extends FragmentActivity implements OnMapReadyCallback ,GoogleMap.OnInfoWindowClickListener, ConnectionHandler.VolleyResponseListener{

    private static final String TAG = MapPage.class.getSimpleName();
    //private List<Stream> AStreams=null;

    private boolean gaugesDisplayed = false;
    private GoogleMap map;
    //private EditText search_view;
    private FloatingActionButton change_base_layer_btn;
    private Button gauge_btn,aStream_btn, plot_wild_stream_btn, plot_stocked_streams,
            plot_nat_btn, search_btn, legend_btn, stocked_water_btn,special_reg_streams_btn;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;    private Stream_Gauge_Data gauge_data;
    public Context mContext;
    public static Searchable_Data allData;
    private LatLng userLoc;
    private BottomSheetBehavior bottomSheet;
    private Marker lcMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_page);
        mContext = getApplicationContext();
        Log.e("In on create", "on create");
        SupportMapFragment mapFragment;

        //sets up map
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        change_base_layer_btn = findViewById(R.id.change_btn);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        bottomSheet = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet));

        init_bottom_sheet_buttons();

        initialize_button_listeners();
        //changes base layer for map cycles through them
    }


    /**
     *
     * @param layer layer to be init
     */
    private void toggleButton(String layer, Button button){

        if (!LayerHandler.getLayerHandlerInstance().isLayerOnMap(layer)) {
            LayerHandler.getLayerHandlerInstance().initializeLayer(layer,map.getCameraPosition().zoom,getLowerLeft(), getUpperRight(), MapPage.this, this);
            button.setTextColor(Color.BLUE);
            Log.e("layer add:", "is layer added");
        } else if (layer != null) {
            try {
                LayerHandler.getLayerHandlerInstance().turnOffLayer(layer);
                button.setTextColor(Color.BLACK);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(mContext, "Layer not loaded yet.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *
     */
    private void init_bottom_sheet_buttons(){
        search_btn = findViewById(R.id.button_search);
        aStream_btn = findViewById(R.id.button_astream);
        gauge_btn =  findViewById(R.id.button_gauges);
        plot_wild_stream_btn =  findViewById(R.id.button_wilderness);
        plot_stocked_streams =  findViewById(R.id.button_stocked_streams);
        plot_nat_btn =  findViewById(R.id.button_natural_repro);
        legend_btn = findViewById(R.id.button_legend);
        stocked_water_btn =findViewById(R.id.button_stocked_water);
        special_reg_streams_btn=findViewById(R.id.button_special_reg_streams);
    }


    /**
     * Renders layer onto map when layer is not yet visible
     * @param layer a geojson layer
     */
    private void add_layer(GeoJsonLayer layer) {
        //determine what coordinates are within the map frame
        //limit to a certain zoom level?
        //layer.addLayerAsync(map.getProjection().getVisibleRegion());
        layer.addLayerToMap();

    }


    /**
     * Updates layer that is visible and renders on map
     * @param layer layer to update
     */
    private void update_layer(GeoJsonLayer layer){
        layer.updateMapAsync(map.getProjection().getVisibleRegion());
    }

    /**
     *
     * @param latlng latlng selected for nav
     */
    private void launchNavigation(LatLng latlng){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+latlng.latitude+","+latlng.longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    public void onResponseSuccessJson(JSONObject response, String tag){

        if(LayerHandler.getLayerHandlerInstance().isLayerOnMap(tag)){
            LayerHandler.getLayerHandlerInstance().addFeatures(response,tag);
        }else {
            LayerHandler.getLayerHandlerInstance().setLayer(tag, new GeoJsonLayer(map, response, LayerHandler.getColor(tag)));
            LayerHandler.getLayerHandlerInstance().turnOnLayer(tag);
        }
    }


    @Override
    public void onResponseFailureJson(VolleyError error, String tag){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//            Toast.makeText(context,
//                    context.getString(R.string.error_network_timeout),
//                    Toast.LENGTH_LONG).show();
            Log.e("timeout","error");
        } else if (error instanceof AuthFailureError) {
            //TODO
            Log.e("AuthFailureError","error");
        } else if (error instanceof ServerError) {
            //TODO\
            Log.e("ServerError","error");
        } else if (error instanceof NetworkError) {
            //TODO
            Log.e("NetworkError","error");
        } else if (error instanceof ParseError) {
            //TODO
            Log.e("ParseError","error");
        }
    }



    /**
     *
     * @param googleMap the map
     */
    @Override
    public void onMapReady(final GoogleMap googleMap) {
            Log.e("waiting", "yes waiting ");
            map = googleMap;
            gauge_data = new Stream_Gauge_Data(mContext,map);

            gauge_data.connect();

            map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    if(lcMarker!=null){
                        lcMarker.remove();
                    }
                    lcMarker = map.addMarker(new MarkerOptions().position(latLng)
                            .title("Marked Location")
                            .snippet("Click for directions\n"+latLng.latitude+","+latLng.longitude)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .visible(true));
                }
            });

            map.setOnMapClickListener(new GoogleMap.OnMapClickListener(){
                @Override
                public void onMapClick(LatLng latLng){
                    if(lcMarker!=null){
                        lcMarker.remove();
                    }
                }
            });

            //info window adapter for custom popup display
            map.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));
            map.setOnInfoWindowClickListener(this);
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

            //call back for when stream gauge data is ready to be displayed
            gauge_data.registerGaugeCallBack(
            new Stream_Gauge_Data.gaugeCallback() {
                @Override
                public void gaugeReady() {
                    Log.e("callback", "gauge data added");
                    gauge_data.addGauges();
                    allData= new Searchable_Data(gauge_data.get_data(),mContext);

                }
            });

            //location stuff sets up then checks permissions
            locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                    //Location Permission already granted
                    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    map.setMyLocationEnabled(true);

            } else {
                //Request Location Permission
                checkLocationPermission();
            }

            //on camera idle to update what part of layer is now visible
            map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    Log.e("MAPZOOM",Float.toString(map.getCameraPosition().zoom));
                    Log.e("oncameraidle", "called");
                    //LayerHandler.getLayerHandlerInstance().updateLayersOnMap(map.getCameraPosition().zoom,getLowerLeft(), getUpperRight(), MapPage.this);
                    LayerHandler.getLayerHandlerInstance().updateLayersWithCache(map.getCameraPosition().zoom,getLowerLeft(), getUpperRight(), MapPage.this, MapPage.this);
                }
            });

            //Polyline listener for kml layers because things got messed up with googles kml feature listener
            //this fixes things hopefully doesnt mess things up
            //TODO: write html parser for stream data
            map.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
                @Override
                public void onPolylineClick(Polyline polyline) {
                    polyLineSelector(polyline);
                }
            });
    }


    /**
     *
     * @param polyline polyline that was clicked
     */
    private void polyLineSelector(Polyline polyline){
        Feature feature;
        Log.e("polyline listener","in here");
        if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.ASTREAMS))
                &&(((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.ASTREAMS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.ASTREAMS).getContainerFeature(polyline)) != null))) {
            show_stream_dialog(feature);

        } else if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.WILDSTREAMS))&& (((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.WILDSTREAMS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.WILDSTREAMS).getContainerFeature(polyline)) != null))) {
            show_stream_dialog(feature);

        }else if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.STOCKEDSTREAMS))
                &&(((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.STOCKEDSTREAMS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.STOCKEDSTREAMS).getContainerFeature(polyline)) != null))) {
            show_stock_stream_dialog(feature);

        } else if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.SPECIALREGSTREAMS))
                &&(((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.SPECIALREGSTREAMS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.SPECIALREGSTREAMS).getContainerFeature(polyline)) != null))) {
            show_stream_dialog(feature);
        }else if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.STOCKEDWATERS))
                &&(((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.STOCKEDWATERS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.STOCKEDWATERS).getContainerFeature(polyline)) != null))) {
            show_stock_stream_dialog(feature);
        }else if ((LayerHandler.getLayerHandlerInstance().isLayerOnMap(LayerHandler.NATREPROSTREAMS))&& (((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.NATREPROSTREAMS).getFeature(polyline)) != null)
                || ((feature = LayerHandler.getLayerHandlerInstance().getLayer(LayerHandler.NATREPROSTREAMS).getContainerFeature(polyline)) != null))) {
            show_stream_dialog(feature);

        }
    }

    /**
     * Shows stream updates called from polyline listener
     * @param feature feature that was clicked
     */
    private void show_stream_dialog(Feature feature){
        Log.i("get feature container", "feature not null");

        Iterable<String> keys = feature.getPropertyKeys();

        final Dialog d = new Dialog(MapPage.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        d.setContentView(R.layout.popup);
        TextView tvName =  d.findViewById(R.id.snippet);

        StringBuilder textToAdd = new StringBuilder("WtrName " + feature.getProperty("WtrName") + "\n");

        for(String e:keys){
            Log.e("key",e);
            if(!e.equals("WtrName"))
                textToAdd.append(e).append(" ").append(feature.getProperty(e)).append("\n");
        }
        tvName.setText(textToAdd.toString());

        d.show();
    }

    /**
     *
     * @param feature stocked stream feature that was clicked
     */
    private void show_stock_stream_dialog(final Feature feature){
        Log.i("get feature container", "feature not null");

        Iterable<String> keys = feature.getPropertyKeys();

        final Dialog d = new Dialog(MapPage.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        d.setContentView(R.layout.popup_w_btn);
        TextView tvName =  d.findViewById(R.id.snippet);

        StringBuilder textToAdd = new StringBuilder("WtrName " + feature.getProperty("WtrName") + "\n");

        for(String e:keys){
            Log.e("key",e);
            if(!e.equals("WtrName"))
                textToAdd.append(e).append(" ").append(feature.getProperty(e)).append("\n");
        }

        tvName.setText(textToAdd.toString());
        Button btn = d.findViewById(R.id.goto_btn);//btn.setText("Stocking Schedule");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //goto here
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fbweb.pa.gov/stocking/TroutStocking_ATW_GIS_RFP.aspx?RFP_WaterID="+ feature.getProperty("RFP_WaterI")));
                startActivity(browserIntent);
            }
        });
        d.show();
    }

    /**
     * Location callback function
     */
    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                //Place current location marker
                userLoc = new LatLng(location.getLatitude(), location.getLongitude());

                //move map camera
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(userLoc, 11));
            }
        }
    };


    /**
     * initialize button listeners
     * all buttons initialized here
     */
    private void initialize_button_listeners(){
        change_base_layer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (map.getMapType()) {
                    case 1:
                        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        break;
                    case 2:
                        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        break;
                    case 3:
                        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        break;
                    case 4:
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            }
        });

        legend_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapPage.this, Legend.class);
                startActivity(intent);
            }
        });


        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                onSearchRequested();
                Log.e("mapPage","search called");
            }
        });

        aStream_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                toggleButton(LayerHandler.ASTREAMS,aStream_btn);
            }

        });
        plot_wild_stream_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButton(LayerHandler.WILDSTREAMS,plot_wild_stream_btn);
            }
        });
        plot_stocked_streams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButton(LayerHandler.STOCKEDSTREAMS,plot_stocked_streams);
            }
        });
        plot_nat_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButton(LayerHandler.NATREPROSTREAMS,plot_nat_btn);
            }
        });
        special_reg_streams_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButton(LayerHandler.SPECIALREGSTREAMS,special_reg_streams_btn);
            }
        });
        stocked_water_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButton(LayerHandler.STOCKEDWATERS,stocked_water_btn);
            }
        });

        gauge_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gauge_data.getGaugeMarkers() != null) {
                    if (!isGaugesOn()) {
                        showMarkers(gauge_data.getGaugeMarkers());
                        //gauge_btn.setText(R.string.hide_gauge);
                        setGaugeDisplay();
                        gauge_btn.setTextColor(Color.BLUE);

                    } else {
                        //hide guages
                        //gauge_btn.setText(R.string.show_gauge);
                        hideMarkers(gauge_data.getGaugeMarkers());
                        setGaugeDisplay();
                        gauge_btn.setTextColor(Color.BLACK);

                    }
                }
                else{Log.e(TAG, "did not display null.");
                    Toast.makeText(mContext, "Gauges not loaded yet.", Toast.LENGTH_SHORT).show();
                    gauge_data.connect();

                }
            }
        });
    }


    /**
     * onIfoWindowClick
     * info window click to start new browser intent for stream guages
     * @param marker marker that is clicked
     */
    @Override
    public void onInfoWindowClick(final Marker marker) {
        //Toast.makeText(this, marker.getTitle(), Toast.LENGTH_LONG).show();
        if (marker.getTitle().equals("Marked Location")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Directions or Copy Coordinates?");
            builder.setMessage("Message");
            builder.setPositiveButton("Directions", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    launchNavigation(marker.getPosition());
                }
            });

            builder.setNegativeButton("Copy LatLng", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //TODO add latlng to clipboard
                    final android.content.ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText("PA Streams", marker.getPosition().latitude+","+marker.getPosition().longitude);
                    clipboardManager.setPrimaryClip(clipData);

                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        } else if (isGaugesOn()) {
            try {
                int ind = gauge_data.getGaugeIndex(marker.getTitle());

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://waterdata.usgs.gov/nwis/uv?site_no=" + gauge_data.getGaugeId(ind)));
                startActivity(browserIntent);
            } catch (Exception e) {
                System.out.println("Not a gauge marker click or no matching title");
            }
        }
    }

    //http://fbweb.pa.gov/stocking/TroutStocking_ATW_GIS_RFP.aspx?RFP_WaterID=1796

    public void showMarkers(List<Marker> list) {
        for (Marker m : list) {
            m.setVisible(true);
        }
    }

    public void hideMarkers(List<Marker> list) {
        for (Marker m : list) {
            m.setVisible(false);
        }
    }

    public void setGaugeDisplay(){
        gaugesDisplayed= !gaugesDisplayed;
    }

    public boolean isGaugesOn(){
        return gaugesDisplayed;
    }

    private LatLng getLowerLeft(){
        return map.getProjection().getVisibleRegion().nearLeft;
    }

    private LatLng getUpperRight(){
        return map.getProjection().getVisibleRegion().farRight;
    }
    @Override
    public void onResume() {
        Log.e("In on resume", "on resume");
        super.onResume();
        searchReturn();
    }

    @Override
    public void onPause() {
        Log.e("In on pause", "on pause");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Log.e("In on destroy", "on destroy");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * checks to see if item was selected in search then updates map to show selection
     */
    private void searchReturn(){

        if(allData!=null&&allData.changed){

            allData.changed = false;
            if(map!=null){
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(getSelectedLoc(), 12));
                switch(allData.which) {
                    case 0:
                        if (!isGaugesOn()) {
                            showMarkers(gauge_data.getGaugeMarkers());
                            setGaugeDisplay();
                            gauge_btn.setTextColor(Color.BLUE);

                        }
                        List<Marker> mark=gauge_data.getGaugeMarkers();
                        //Finds which marker to display
                        for(Marker m:mark){
                            if(m.getTitle().equals(getSelectedName())){
                                m.showInfoWindow();
                            }
                        }
                        break;
                    case 1://streams nothing special to do right now todo maybe turn only that stream on
                        break;
                    case 2:
                        LatLng ll = getSelectedLoc();
                        lcMarker = map.addMarker(new MarkerOptions().position(ll)
                                .title("Marked Location")
                                .snippet("Click for directions\n"+ll.latitude+","+ll.longitude)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .visible(true));
                        break;
                }
            }
        }
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.e("check loc", "Alert dialog");

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapPage.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                Log.e("check loc", "No alert");

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.e("check loc", "checking loc1");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                        map.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



}
