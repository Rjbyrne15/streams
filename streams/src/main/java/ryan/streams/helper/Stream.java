package ryan.streams.helper;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * Created by Ryan on 3/16/2018.
 */

public class Stream {
    private String name, county,type;
    private LatLng latlon;


    public Stream(String type,String name,String county, double lat, double lon) {

        this.name = name;
        this.county = county;
        this.latlon = new LatLng(lat, lon);
    }



    public String getName() {
        return this.name;
    }

    public String getCounty() {
        return this.county;
    }

    public LatLng getLatlon() {
        return this.latlon;
    }



}
