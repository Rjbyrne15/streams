package ryan.streams.helper;

import java.util.ArrayList;

public interface MapViewCache {

    ArrayList<PreviousView> getNewViewsToRequest(String key, PreviousView view);
}
