package ryan.streams.helper;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewCache {

    private static ViewCache viewCache = null;
    private Map<String, ArrayList<PreviousView>> cache = null;

    ViewCache() {
        cache = new HashMap<>();
    }

    public static ViewCache getViewCacheInstance() {
        if (viewCache == null) {
            viewCache = new ViewCache();
            return viewCache;
        }
        return viewCache;
    }

    public void removeAll(String tag) {
        if(cache.containsKey(tag)){
            cache.put(tag,null);
        }
    }

    public void add(String s,PreviousView view) {
        if(!cache.containsKey(s)||cache.get(s)==null){
            cache.put(s,new ArrayList<PreviousView>());
        }
        cache.get(s).add(view);
    }

    public List<PreviousView> getNewViewsToRequest(String tag, PreviousView view) {
        ArrayList<PreviousView> newRequests = new ArrayList<>();
        PreviousView upper = null;
        PreviousView lower = null;
        int idx = -9;
        double overlap = 0.0;

        //find the largest overlap with an old view
        if(cache.get(tag)==null){
            return null;
        }
        for (int i = 0; i < cache.get(tag).size(); i++) {
            Log.e("New long", "LONG: "+view.getLowerLeft().longitude);
            Log.e("Old long", "LONG: "+cache.get(tag).get(i).getLowerLeft().longitude);
            if (cache.get(tag).get(i).getTag().equals(view.getTag())) {
                if (view.getZoomLevel() >= cache.get(tag).get(i).getZoomLevel()) {
                    if(isContained(view,cache.get(tag).get(i))) {
//                        newRequests.add(view);
//                        //add(view);
//                        return newRequests;
                        return null;
                    }
                    double newOverlap = calcOverlap(cache.get(tag).get(i), view);
                    if(newOverlap==-99.0){
                        newRequests.add(view);
                        //add(view);
                        return null;
                    }
                    if (newOverlap > overlap) {
                        idx = i;
                        overlap = newOverlap;
                    }
                }
            }
        }

        //no overlap get entire frame dont calculate new views
        if (idx == -9 || overlap ==0.0) {
            newRequests.add(view);
            //add(view);
            Log.e("ViewCache","Returning -9 or 0");
            return newRequests;
        }

        if (view.getLowerLeft().latitude < cache.get(tag).get(idx).getLowerLeft().latitude) {//below
            if (view.getLowerLeft().longitude < cache.get(tag).get(idx).getLowerLeft().longitude) {//left
                upper = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(view.getLowerLeft().latitude, view.getLowerLeft().longitude),
                        new LatLng(view.getUpperRight().latitude, cache.get(tag).get(idx).getLowerLeft().longitude));
                lower = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(view.getLowerLeft().latitude, cache.get(tag).get(idx).getLowerLeft().longitude),
                        new LatLng(view.getLowerLeft().latitude, cache.get(tag).get(idx).getUpperRight().longitude));
            } else if (view.getLowerLeft().longitude > cache.get(tag).get(idx).getLowerLeft().longitude) {//right
                upper = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(view.getLowerLeft().latitude, cache.get(tag).get(idx).getUpperRight().longitude),
                        new LatLng(view.getUpperRight().latitude, cache.get(tag).get(idx).getLowerLeft().longitude));
                lower = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(view.getLowerLeft().latitude, view.getLowerLeft().longitude),
                        new LatLng(cache.get(tag).get(idx).getLowerLeft().latitude, cache.get(tag).get(idx).getUpperRight().longitude));

            } else {
                 newRequests.add(view);
                 //newRequests.add(null);
                //add(view);
                Log.e("viewCache","In the else");
                return newRequests;
            }

        } else {//above
            if (view.getLowerLeft().longitude < cache.get(tag).get(idx).getLowerLeft().longitude) {//left
                upper = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(cache.get(tag).get(idx).getUpperRight().latitude, cache.get(tag).get(idx).getLowerLeft().longitude),
                        new LatLng(view.getUpperRight().latitude, view.getUpperRight().longitude));
                lower = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(view.getLowerLeft().latitude, view.getLowerLeft().longitude),
                        new LatLng(view.getUpperRight().latitude, cache.get(tag).get(idx).getLowerLeft().longitude));
            } else  if (view.getLowerLeft().longitude > cache.get(tag).get(idx).getLowerLeft().longitude){//right
                upper = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(cache.get(tag).get(idx).getUpperRight().latitude, view.getLowerLeft().longitude),
                        new LatLng(view.getUpperRight().latitude, cache.get(tag).get(idx).getUpperRight().longitude));
                lower = new PreviousView(view.getTag(), view.getZoomLevel(), new LatLng(cache.get(tag).get(idx).getUpperRight().latitude, view.getLowerLeft().longitude),
                        new LatLng(view.getUpperRight().latitude, view.getUpperRight().longitude));
            } else {
                newRequests.add(view);
//                newRequests.add(null);
                //add(view);
                Log.e("viewCache","In the else1");
                return newRequests;
            }
        }


        newRequests.add(upper);
        newRequests.add(lower);
        //ArrayList<PreviousView> reqs = checkViews(newRequests);


        //cache.add(view);
        return newRequests;
    }

    private void add(String tag, ArrayList<PreviousView> pv){
        if(!cache.containsKey(tag)){
            cache.put(tag,new ArrayList<PreviousView>());
        } else if(cache.get(tag)==null){
            cache.put(tag,new ArrayList<PreviousView>());
        }

        for(PreviousView p : pv){
            cache.get(tag).add(p);
        }
    }



    public List<PreviousView> checkViews(String tag,List<PreviousView> previousViews) {
        List<PreviousView> pvs;
        List<PreviousView> retList = new ArrayList<>();
        Log.e("checkview","before loop");
        for (int i =0; i<previousViews.size();i++) {
            PreviousView pv = previousViews.get(i);
            Log.e("checkview","in loop "+ previousViews.size());
            pvs = getNewViewsToRequest(tag,pv);
            if (pvs != null) {
                if (pvs.size() > 1) {
//                    ArrayList<PreviousView> cvViews = checkViews(pvs);
//                    if(cvViews!=null) {
                        for (PreviousView p : pvs) {
                            if(p!=null) {
                                previousViews.add(p);
                            }
                        }
//                    }
                } else {
                    if (pvs.size() == 1) {
                        add(tag, pvs.get(0));
                        retList.add(pvs.get(0));
                        Log.e("VIEWCACHE","ADDED to cache in check views");
                    } else {
                        Log.e("VIewCache", "size is 0");
                    }
                }
            } else {
                Log.e("ViewCache","pvs is null!!!!");
            }
        }
        //add(retList);
        return retList;
    }


    private double calcOverlap(PreviousView pv1, PreviousView pv2) {

        if(!doOverlap(pv2,pv1)){
            Log.e("do overlap", "Do not overlap");
            return 0.0;
        }
        double old = Math.abs(pv1.getLowerLeft().latitude - pv1.getUpperRight().latitude) *
                Math.abs(pv1.getLowerLeft().longitude - pv1.getUpperRight().longitude);

        double newV = Math.abs(pv2.getLowerLeft().latitude - pv2.getUpperRight().latitude) *
                Math.abs(pv2.getLowerLeft().longitude - pv2.getUpperRight().longitude);

        double areaO = Math.abs((Math.min(pv1.getUpperRight().longitude, pv2.getUpperRight().longitude) -
                Math.max(pv1.getLowerLeft().longitude, pv2.getLowerLeft().longitude))) *
                Math.abs((Math.min(pv1.getUpperRight().latitude, pv2.getUpperRight().latitude) -
                        Math.max(pv1.getLowerLeft().latitude, pv2.getLowerLeft().latitude)));
        Log.e("Area is ","a  "+areaO);
//        //if overlapping area is less than 10% return 0 and dont consider it overlapping
        Log.e("Area 1 is","  "+ old);
//        if (areaO / area1 < .2 || areaO / area1 ==1.0) {
//
//            return 0;
//        }
        if(areaO/old == 1.0){
            Log.e("area ", "area is equal0");
            return -99.0;
        }
        if (areaO < .02) {

            return 0;
        }
        return areaO;
    }

    public boolean isContained(PreviousView pv1, PreviousView pv2){
        Log.e("contained","In the method ");
        if(doOverlap(pv1,pv2)){
            if (pv1.getLowerLeft().longitude >= pv2.getLowerLeft().longitude - .02
                    && pv1.getUpperRight().longitude <= pv2.getUpperRight().longitude + .02
                    && pv1.getLowerLeft().latitude >= pv2.getLowerLeft().latitude - .02
                    && pv1.getUpperRight().latitude <= pv2.getUpperRight().latitude + .02) {
                Log.e("contained", "yes ");
                return true;
            }
        }


        return false;
    }

    boolean doOverlap(PreviousView pv1, PreviousView pv2)
    {
        // If one rectangle is on left side of other
        if(pv1.getZoomLevel()<=pv2.getZoomLevel()) {
            if (pv1.getLowerLeft().longitude > pv2.getUpperRight().longitude - .01 || pv2.getLowerLeft().longitude > pv1.getUpperRight().longitude - .01)
                return false;

            // If one rectangle is above other
            if (pv1.getLowerLeft().latitude > pv2.getUpperRight().latitude - .01 || pv2.getLowerLeft().latitude > pv1.getUpperRight().latitude - .01)
                return false;

            Log.e("Overlap", "Yes they do overlap");
            return true;
        }
        return false;
    }

    private void cleanCache(){

    }

}






















