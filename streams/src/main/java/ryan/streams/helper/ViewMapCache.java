package ryan.streams.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewMapCache implements MapViewCache {

    private static ViewMapCache viewCache = null;
    private Map<String, PreviousView> cache = null;

    ViewMapCache() {
        cache = new HashMap<>();
    }

    public static ViewMapCache getMapViewCacheInstance() {
        if (viewCache == null) {
            viewCache = new ViewMapCache();
            return viewCache;
        }
        return viewCache;
    }

    private void add(String s, PreviousView v){
        cache.put(s,v);
    }

    public ArrayList<PreviousView> getNewViewsToRequest(String key, PreviousView view) {
        return null;
    }


}
