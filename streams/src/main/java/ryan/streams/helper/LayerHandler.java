package ryan.streams.helper;

import android.app.Activity;
import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.data.geojson.GeoJsonLayer;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ryan.streams.activities.MapPage;

public class LayerHandler {
    private static LayerHandler layerHandler = null;

    public static final String ASTREAMS="astreams";
    public static final String WILDSTREAMS="wildstreams";
    public static final String NATREPROSTREAMS="natreprostreams";
    public static final String STOCKEDSTREAMS="stockedstreams";
    public static final String STOCKEDWATERS="stockedwaters";
    public static final String SPECIALREGLAKES="specialreglakes";
    public static final String SPECIALREGSTREAMS="specialregstreams";
    private static ConnectionHandler conHandler;

    private static int ASTREAMSCOLOR = Color.argb(255, 27, 158, 119);
    private static int WILDSTREAMSCOLOR = Color.argb(255, 255, 128, 0);
    private static int NATREPROSTREAMSOLOR = Color.argb(255, 27, 158, 119);
    private static int STOCKEDSTREAMSCOLOR = Color.argb(255, 255, 0, 0);
    private static int STOCKEDWATERSCOLOR = Color.argb(255, 0, 0, 255);
    private static int SPECIALREGLAKESCOLOR = Color.argb(255, 255, 255, 0);
    private static int SPECIALREGSTREAMSCOLOR = Color.argb(255, 0, 204, 255);


    private static int[] colors ={ASTREAMSCOLOR,WILDSTREAMSCOLOR,NATREPROSTREAMSOLOR,STOCKEDSTREAMSCOLOR,STOCKEDWATERSCOLOR,SPECIALREGLAKESCOLOR,SPECIALREGLAKESCOLOR,SPECIALREGSTREAMSCOLOR};
    private static String[] layerKeys = {ASTREAMS,WILDSTREAMS,NATREPROSTREAMS,STOCKEDSTREAMS,STOCKEDWATERS,SPECIALREGLAKES,SPECIALREGSTREAMS};
    private static Map<String, GeoJsonLayer> layers = new HashMap<>();
    private static Map<String, Integer> layerColors = new HashMap<>();

    public LayerHandler(){
        setLayerColors();
        for(String key : layerKeys){
            layers.put(key,null);
        }

    }

    public static LayerHandler getLayerHandlerInstance(){
        if (layerHandler ==null){
            layerHandler = new LayerHandler();
            return layerHandler;
        }
        return layerHandler;
    }

    public LayerHandler(VisibleRegion visRegion){

    }

    public boolean isLayerOnMap(String tag){
        if(layers.containsKey(tag)&&layers.get(tag)!=null){
            return layers.get(tag).isLayerOnMap();
        }
        return false;
    }
    public void InitializeLayers(){
        //do api calls
    }

    public void setLayer(String key, GeoJsonLayer layer){
        if (layers.containsKey(key)) {
            layers.put(key,layer);
        }
    }

    public void turnOnLayer(String key){
        if(layers.containsKey(key)&&layers.get(key)!=null){
            getLayer(key).addLayerToMap();
        }
    }

    public void turnOffLayer(String key){
        if(layers.containsKey(key)&&layers.get(key)!=null){
            getLayer(key).removeLayerFromMap();
            layers.put(key,null);
            ViewCache.getViewCacheInstance().removeAll(key);
        }
    }

    public GeoJsonLayer getLayer(String key) {
        if (layers.containsKey(key)&&layers.get(key)!=null) {
            return layers.get(key);
        }

        return null;
    }

    public void updateLayersOnMap(float zoom, LatLng ll, LatLng ur, ConnectionHandler.VolleyResponseListener volleyListener, Activity act){
        for(String key : layerKeys){
            if(layers.get(key)!=null && layers.get(key).isLayerOnMap()) {
                ConnectionHandler.getConnectionHandlerInstance().getStreamsByLocAndType(key, zoom, ll, ur, volleyListener, act);
            }
        }
    }

    public void initializeLayer(String key, float zoom, LatLng ll, LatLng ur, ConnectionHandler.VolleyResponseListener volleyListener,Activity act){
        if(layers.containsKey(key)){
            ViewCache.getViewCacheInstance().add(key,new PreviousView(key,zoom,ll,ur));
            ConnectionHandler.getConnectionHandlerInstance().getStreamsByLocAndType(key, zoom, ll, ur, volleyListener, act);
        }
    }

    public void addFeatures(JSONObject jobj, String tag){
        if(layers.containsKey(tag)){
            layers.get(tag).addFeatures(jobj);
        }
    }

    public void updateLayersWithCache(float zoom, LatLng ll, LatLng ur, ConnectionHandler.VolleyResponseListener volleyListener, Activity act){
        for(String key : layerKeys){
            if(layers.get(key)!=null && layers.get(key).isLayerOnMap()) {
                //ConnectionHandler.getConnectionHandlerInstance().getStreamsByLocAndType(key, zoom, ll, ur, volleyListener);

                //ConnectionHandler.getConnectionHandlerInstance().updateMultipleViews(ViewCache.getViewCacheInstance().getNewViewsToRequest(new PreviousView(key,zoom,ll,ur)),volleyListener);
                List<PreviousView> l = new ArrayList<>();
                l.add(new PreviousView(key,zoom,ll,ur));
                ConnectionHandler.getConnectionHandlerInstance().updateMultipleViews(ViewCache.getViewCacheInstance().checkViews(key ,l),volleyListener, act);
                //ViewCache.getViewCacheInstance().add(new PreviousView(key,zoom,ll,ur));
            }
        }

    }

    public static int getColor(String key){
        return layerColors.get(key);
    }

    private void setLayerColors(){
        for(int i =0;i<layerKeys.length&&i<colors.length;i++){
            layerColors.put(layerKeys[i],colors[i]);
        }
    }


}
