package ryan.streams.helper;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;

import ryan.streams.objects.Gauge;

public class Searchable_Data {


    private static ArrayList<Gauge> gauges;
    private static LatLng selected_loc;
    private static String selected_name;
    private static SQLiteHandler sqlhandler;
    public boolean changed = false;
    public int which;

    //TODO pass gauge Markers list maybe

    public Searchable_Data(ArrayList<Gauge> g,Context mcontext){
        gauges =g;

            sqlhandler= new SQLiteHandler(mcontext);
            //sqlhandler.dropTables();
            //sqlhandler.getCities("holland");

    }

    public static ArrayList<Gauge> getGauges() {
        return gauges;
    }

    public static void setGauges(ArrayList<Gauge> g){
        gauges = g;
    }

    public static LatLng getSelectedLoc(){
        if(selected_loc!=null)
            return selected_loc;
        return null;
    }

    public static void setSelectedName(String n){
        selected_name=n;
    }


    public static String getSelectedName(){
        return selected_name;
    }


    public static void setSelectedLoc(LatLng s){
        selected_loc=s;
    }



    public static ArrayList<HashMap<String,String>> getCities(String city){
        return sqlhandler.getCities(city);
    }
}
