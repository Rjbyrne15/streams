package ryan.streams.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import ryan.streams.BuildConfig;
import ryan.streams.R;
import ryan.streams.activities.MapPage;


//./adb -d uninstall ryan.academy_laundry2
public class  SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "uscities";

    // City table name
    private static final String TABLE_CITIES = "cities";


    //private static String DB_PATH ="/data/data/"+ BuildConfig.APPLICATION_ID+"/databases/";

    // Login table name

    private Context mycontext;
    private SQLiteDatabase myDataBase;


    // Cities Table Columns names
    private static final String CITY = "city";
    private static final String LAT = "lat";
    private static final String LNG = "lng";
    private static final String STATE_ID = "state_id";
    private static final String STATE = "state_name";
    private static final String COUNTY = "county_name";
    private static final String ZIP ="zips";



    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mycontext = context;

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

            String CREATE_CITIES_TABLE = "CREATE TABLE " + TABLE_CITIES + "("
                    + CITY + " TEXT," + STATE_ID + " TEXT,"
                    + STATE + " TEXT," + COUNTY + " TEXT," + LAT + " TEXT,"
                    + LNG + " TEXT," + ZIP + " TEXT" + ")";
            db.execSQL(CREATE_CITIES_TABLE);
            try {
                this.copyDB(db);
            } catch (IOException e) {
                Log.e("Create db", "Copy error");
                e.printStackTrace();
            }

            //String CREATE_CUSTOMER_TABLE="CREATE TABLE "+TABLE_CUSTOMERS + "( "+ KEY_CUSTOMER +" TEXT)";
            //db.execSQL(CREATE_CUSTOMER_TABLE);
            Log.d(TAG, "Database tables created");

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITIES);
        // Create tables again
        onCreate(db);
    }


    public void dropTables() {
        // Drop older table if existed
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITIES);
        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     */
    public void addC(String city, String state_id, String state, String county, String lat,String lng, String zip) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CITY, city); // Name
        values.put(STATE_ID, state_id); // Email
        values.put(STATE, state); // Email
        values.put(COUNTY, county); // group
        values.put(LAT, lat); // Created At
        values.put(LNG, lng); // group
        values.put(ZIP, zip); //

        // Inserting Row
        long id = db.insert(TABLE_CITIES, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New city inserted into sqlite: " + id);
    }
    /**
     * Getting user data from database
     */
    public HashMap<String, String> getCityDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_CITIES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("grouptype",cursor.getString(4));
            user.put("created_at", cursor.getString(5));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }
    /**
     * Re crate database Delete all tables and create them again
     */
    public void deleteC() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows

        db.delete(TABLE_CITIES, null, null);
        // query to obtain the names of all tables in your database
       /* Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        List<String> tables = new ArrayList<>();
// iterate over the result set, adding every table name to a list
        while (c.moveToNext()) {
            tables.add(c.getString(0));
        }
// call DROP TABLE on every table name
        for (String table : tables) {
            String dropQuery = "DROP TABLE IF EXISTS " + table;
            Log.e("tables", table);
            db.execSQL(dropQuery);
        }*/
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }


    public void copyDB(final SQLiteDatabase db) throws IOException {
        //FileReader file = new FileReader("uscities.csv");


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                InputStream is =  mycontext.getResources().openRawResource(R.raw.uscities1_5);
                InputStreamReader file = new InputStreamReader(is);
                BufferedReader buffer = new BufferedReader(file);
                String line = "";
                String columns = "'"+CITY+ "','" +STATE_ID+ "','" +STATE+ "','" +COUNTY+ "','" +LAT+ "','" +LNG+ "','" +ZIP+"'";
                String str1 = "INSERT INTO " + TABLE_CITIES + " (" + columns + ") values(";
                String str2 = ");";
//        SQLiteDatabase db = this.getReadableDatabase();
                try {
                db.beginTransaction();

                    line = buffer.readLine();

                while ((line = buffer.readLine()) != null) {
                    StringBuilder sb = new StringBuilder(str1);
                    String[] str = line.split(",");
                    //Log.e("length of str", String.valueOf(str.length)+" " + str[0]+str[1]);
                    sb.append("'" + str[0].toLowerCase() + "','");
                    sb.append(str[1] + "','");
                    sb.append(str[2] + "','");
                    sb.append(str[3] + "','");
                    sb.append(str[4] + "','");
                    sb.append(str[5] + "','");
                    sb.append(str[6] + "'");
                    sb.append(str2);
                    db.execSQL(sb.toString());

                }
                db.setTransactionSuccessful();
                db.endTransaction();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //db.close();
    }


    public ArrayList<HashMap<String,String>> getCities(String city) {
        HashMap<String, String> City = new HashMap<String, String>();
        ArrayList<HashMap<String,String>> cities = new ArrayList<HashMap<String, String>>();
Log.e("GetCities ","in GetCities");
        String selectQuery = "SELECT * FROM " +TABLE_CITIES +" WHERE city LIKE \"%"+city+"%\"" + " COLLATE NOCASE";

        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        do {
            if (cursor.getCount() > 0) {
                City.put("city", cursor.getString(0));
                City.put("state_id",cursor.getString(1));
                City.put("lat", cursor.getString(4));
                City.put("lng", cursor.getString(5));
                //user.put("grouptype",cursor.getString(4));
                //user.put("created_at", cursor.getString(5));
                cities.add(City);
                City = new HashMap<String, String>();
                //Log.e("city", City.toString());
            }
        }while(cursor.moveToNext());
        cursor.close();
        db.close();
        // return user
        Log.e(TAG, "Fetching cities from Sqlite: " + City.toString());

        return cities;
    }

}


