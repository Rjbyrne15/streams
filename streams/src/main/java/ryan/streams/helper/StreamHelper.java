package ryan.streams.helper;

import java.util.ArrayList;
import java.util.HashMap;

import ryan.streams.objects.StreamIndex;

public class StreamHelper {

    private HashMap<String, ArrayList<StreamIndex>> streamCategoryHashMap;
    public StreamHelper(){
        this.streamCategoryHashMap = new HashMap<String, ArrayList<StreamIndex>>();
    }
    public StreamHelper(String streamType){
        this.streamCategoryHashMap = new HashMap<String, ArrayList<StreamIndex>>();
        this.streamCategoryHashMap.put(streamType, new ArrayList<StreamIndex>());
    }

    public void addStreamType(String streamType){
        this.streamCategoryHashMap.put(streamType, new ArrayList<StreamIndex>());

    }

    public void addStream(StreamIndex si){
        if(!this.streamCategoryHashMap.containsKey(si.getClassification())){
            this.streamCategoryHashMap.put(si.getClassification(),new ArrayList<StreamIndex>());
        }
        this.streamCategoryHashMap.get(si.getClassification()).add(si);
    }

    public ArrayList<StreamIndex> getClassificationArray(String type){
        return this.streamCategoryHashMap.get(type);
    }


}
