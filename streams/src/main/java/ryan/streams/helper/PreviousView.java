package ryan.streams.helper;

import com.google.android.gms.maps.model.LatLng;

public class PreviousView {

    private float zoomLevel;
    private LatLng lowerLeft;
    private LatLng upperRight;
    String tag;

    public PreviousView(String tag, float zoomLevel, LatLng lowerLeft, LatLng upperRight){
        this.tag=tag;
        this.zoomLevel = zoomLevel;
        this.lowerLeft = lowerLeft;
        this.upperRight = upperRight;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public float getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(long zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public LatLng getLowerLeft() {
        return lowerLeft;
    }

    public void setLowerLeft(LatLng lowerLeft) {
        this.lowerLeft = lowerLeft;
    }

    public LatLng getUpperRight() {
        return upperRight;
    }

    public void setUpperRight(LatLng upperRight) {
        this.upperRight = upperRight;
    }
}
