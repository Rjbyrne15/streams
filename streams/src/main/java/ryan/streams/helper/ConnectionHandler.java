package ryan.streams.helper;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ryan.streams.activities.MapPage;
import ryan.streams.app.AppController;

public class ConnectionHandler {
    //private String URL="http://10.0.0.18:8080/";
    private String URL="http://ec2-18-217-53-122.us-east-2.compute.amazonaws.com:8080/";

    private VolleyResponseListener volleyResponseListener;
    private static ConnectionHandler connectionHandler = null;

    public ConnectionHandler(){

    }

    public static ConnectionHandler getConnectionHandlerInstance(){
        if(connectionHandler == null){
            connectionHandler = new ConnectionHandler();
        }
        return connectionHandler;
    }

    public void getStreamsByLocAndType(final String tag,float zoomlevel, LatLng lll, LatLng llu,VolleyResponseListener volleyListener, Activity con){
        volleyResponseListener=volleyListener;
        if(lll!=null && llu!=null) {
            String androidId = Settings.Secure.getString(con.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            String request = URL + "app/get/" + tag +"/zoom/"+zoomlevel+ "/loc/" + lll.latitude + "/" + lll.longitude + "/" + llu.latitude + "/" + llu.longitude + "/"+androidId+"/";
            String tag_json_obj = "json_obj_req";
//String request = "http://10.0.0.18:8080/app/getAStreams/loc/40.441057/-75.366067/40.690223/-75.130598";
            //String request = "http://10.0.0.18:8080/app/getAStreams";
//        ProgressDialog pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Loading...");
//        pDialog.show();
            //if(AppController.getInstance().){
            cancelAnyRequests(tag_json_obj);


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    request, new JSONObject(),
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("volley", response.toString());

                            volleyResponseListener.onResponseSuccessJson(response, tag);
                            //pDialog.hide();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("volley", "Error: " + error.getMessage());
                    volleyResponseListener.onResponseFailureJson(error, tag);
                    // hide the progress dialog
                    // pDialog.hide();
                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            Log.e("volley", "Locations are null");
        }
        //make request
    }


    public void updateMultipleViews(List<PreviousView> previousViews, VolleyResponseListener volleyListener, Activity con){
        volleyResponseListener=volleyListener;
        String androidId = Settings.Secure.getString(con.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        for(PreviousView pv : previousViews) {

            String request = URL + "app/get/" + pv.getTag() + "/zoom/" + pv.getZoomLevel() + "/loc/" + pv.getLowerLeft().latitude + "/" + pv.getLowerLeft().longitude + "/" +
                    pv.getUpperRight().latitude + "/" + pv.getUpperRight().longitude + "/"+androidId+"/";
            String tag_json_obj = "json_obj_req";
            final String tmpTag = pv.getTag();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    request, new JSONObject(),
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("volley", response.toString());

                            volleyResponseListener.onResponseSuccessJson(response, tmpTag);
                            //pDialog.hide();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("volley", "Error: " + error.getMessage());
                    volleyResponseListener.onResponseFailureJson(error, tmpTag);
                    // hide the progress dialog
                    // pDialog.hide();
                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }

    }


    public void cancelAnyRequests(String tag){
        AppController.getInstance().cancelPendingRequests(tag);
    }


    public interface VolleyResponseListener {
         void onResponseSuccessJson(JSONObject response, String tag);
         void onResponseFailureJson(VolleyError error, String tag);
    }




}
