package ryan.streams.helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ryan.streams.app.AppController;
import ryan.streams.objects.Gauge;


public class Stream_Gauge_Data {


    //private ProgressDialog pDialog;
    private String TAG = "STREAM_GUAGE_DATA";
    private String URL_PA_GAUGES = "https://waterservices.usgs.gov/nwis/iv/?format=json,1.1&siteType=ST&siteStatus=active&stateCd=pa";
    private ArrayList<Gauge> gauges;
    private GoogleMap map;
    private Context mContext;
    private  List<Gauge> lGauges = null;
    private  List<Marker> gaugeMarkers;
    private gaugeCallback gaugecallback;




    public Stream_Gauge_Data(Context nContext, GoogleMap nmap) {
        gauges = new ArrayList<Gauge>();
        map = nmap;
        mContext=nContext;

    }



    public List<Marker> getGaugeMarkers(){return gaugeMarkers;}





    public int getGaugeIndex(String gName){
        int index=0;
        for(Gauge g: lGauges){
            if(g.getName().equals(gName)) {return index;}
            index++;
        }
        return index;
    }




    public void connect() {
        String tag_string_req = "req_update_streams";


        StringRequest strReq = new StringRequest(Request.Method.POST,
                URL_PA_GAUGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.d(TAG, "Get Update Stream Gauge Response: " + response.toString());
                try {
                    parseJSON(response);
                    Log.e(TAG, "After parsing");

                    //setGauges(gauges);
                    lGauges=gauges;
                    //addGauges();
                    gaugecallback.gaugeReady();
                } catch (JSONException jex) {
                    Log.e(TAG, "JSON PARSING ERROR");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Stream  Gaugeupdate Error: " + error.getMessage());
                Toast.makeText(mContext,
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        // Adding request to request queue
        if(strReq!=null) {
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        } else
            Log.e(tag_string_req, "null here not good");


    }



    public String getGaugeId(int ind){
        return lGauges.get(ind).getId();
    }
//Todo: create call back for addgauges



    public void registerGaugeCallBack(gaugeCallback call){
        gaugecallback = call;
    }



    public interface gaugeCallback{
        void gaugeReady();
    }

    public ArrayList<String> get_gauge_names(){
        ArrayList<String> names = new ArrayList<String>();
        if(!gauges.isEmpty())
            for(Gauge g:gauges){
                names.add(g.getName());
            }
            return names;
    }


    public ArrayList<Gauge> get_data() {
        return gauges;
    }



    public void addGauges(){
        if(lGauges != null) {
            gaugeMarkers = new ArrayList<Marker>();

            for (Gauge gauge :  lGauges) {
                gaugeMarkers.add( map.addMarker(new MarkerOptions().position(gauge.getLatlon())
                        .title(gauge.getName())
                        .snippet("Flow Rate: " + gauge.getFlow_rate() + "\nHeight: " +
                                gauge.getHeight() + "\nTime: " + gauge.getTime())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        .visible(false)));

            }
            //map.moveCamera(CameraUpdateFactory.newLatLng(lGauges.get(0).getLatlon()));
        }
    }



    private void parseJSON(String str) throws JSONException {
        JSONObject json = new JSONObject(str);
        Log.e(TAG, "HERE 0.0");
        JSONArray timeseries = json.getJSONObject("value").getJSONArray("timeSeries");

        for (int i = 0; i < timeseries.length() - 1; i++) {
            //Log.e(TAG, "HERE 1");
            JSONObject jgauge = timeseries.getJSONObject(i);

            //['value']['timeSeries'][x]['sourceInfo']['siteCode'][0]['value']
            String gid = jgauge.getJSONObject("sourceInfo").getJSONArray("siteCode").getJSONObject(0).getString("value");
            String gsitename = jgauge.getJSONObject("sourceInfo").getString("siteName");
            String glat = jgauge.getJSONObject("sourceInfo").getJSONObject("geoLocation").getJSONObject("geogLocation").getString("latitude");
            String glon = jgauge.getJSONObject("sourceInfo").getJSONObject("geoLocation").getJSONObject("geogLocation").getString("longitude");
            String gname = jgauge.getJSONObject("variable").getString("variableName");
            String gval = jgauge.getJSONArray("values").getJSONObject(0).getJSONArray("value").getJSONObject(0).getString("value");
            String gdate = jgauge.getJSONArray("values").getJSONObject(0).getJSONArray("value").getJSONObject(0).getString("dateTime");
            String gunit = jgauge.getJSONObject("variable").getJSONObject("unit").getString("unitCode");
            String gdesc = jgauge.getJSONObject("variable").getString("variableDescription");
            boolean exists = false;
            if (!gauges.isEmpty()) {
                for (Gauge g : gauges) {

                    if (g.getName().equals(gsitename)) {
                        exists = true;
                        if (gdesc.equals("Gage height, feet")) {
                            g.setHeight(gval);
                            g.setH_desc(gdesc);
                            g.setH_unit(gunit);
                        } else if (gdesc.equals("Discharge, cubic feet per second")) {
                            g.setFlow_rate(gval);
                            g.setR_desc(gdesc);
                            g.setR_unit(gunit);
                        }
                    }
                }
                if (!exists) {
                    Gauge ng = new Gauge(gid, gsitename, glat, glon, gdate);
                    if (gdesc.equals("Gage height, feet")) {
                        ng.setHeight(gval);
                        ng.setH_desc(gdesc);
                        ng.setH_unit(gunit);
                    } else if (gdesc.equals("Discharge, cubic feet per second")) {
                        ng.setFlow_rate(gval);
                        ng.setR_desc(gdesc);
                        ng.setR_unit(gunit);
                    }
                    gauges.add(ng);
                }

            } else {
                Gauge ng = new Gauge(gid, gsitename, glat, glon, gdate);
                if (gdesc.equals("Gage height, feet")) {
                    ng.setHeight(gval);
                    ng.setH_desc(gdesc);
                    ng.setH_unit(gunit);
                } else if (gdesc.equals("Discharge, cubic feet per second")) {
                    ng.setFlow_rate(gval);
                    ng.setR_desc(gdesc);
                    ng.setR_unit(gunit);
                }
                gauges.add(ng);
            }

        }

    }

}
