package ryan.streams.objects;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class Stream {
    private String name, county,type;
    private LatLng latlon;
    private ArrayList<LatLng> coordinates;

    public Stream(){}


    public Stream(String type,String name,String county, double lat, double lon) {

        this.name = name;
        this.county = county;
        this.latlon = new LatLng(lat, lon);
    }



    public String getName() {
        return this.name;
    }

    public String getCounty() {
        return this.county;
    }

    public LatLng getLatlon() {
        return this.latlon;
    }



}
