package ryan.streams.objects;

import com.google.android.gms.maps.model.LatLng;

public class FlyShop {
    private String shopName;
    private String phoneNumber;
    private String webSite;
    private String address;
    private LatLng latlng;
    private double lat;
    private double lng;

    public FlyShop(){}

    public String getShopName() {
        return shopName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWebSite() {
        return webSite;
    }

    public String getAddress() {
        return address;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setShopName(String name) {
        this.shopName = shopName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLatlng(LatLng latlng) {
        this.latlng = latlng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


}
