package ryan.streams.objects;

import com.google.android.gms.maps.model.LatLng;

public class StreamIndex {
    //This class is to index streams in db will need api. This will lessen memory usage on the phone and hopefully push most work to the server.

    // only basic info needed initial index info sent from server or stored locally?


    int key;
    String classification;
    LatLng loc;
    String name;

    /**
     *Class to be able to determine what needs to be queried from db while trying to keep memory usage low.
     * @param name- name of stream
     * @param loc- location to be able to check if needed to retrieve
     * @param classification- stream type
     * @param key - db key
     */
    public StreamIndex(String name, LatLng loc, String classification, int key ){

        this.key=key;
        this.name=name;
        this.classification=classification;
        this.loc=loc;
    }


    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public LatLng getLoc() {
        return loc;
    }

    public void setLoc(LatLng loc) {
        this.loc = loc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
