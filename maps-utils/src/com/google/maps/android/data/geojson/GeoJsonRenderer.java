package com.google.maps.android.data.geojson;

import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.data.DataPolygon;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.Renderer;
import com.google.maps.android.data.kml.KmlPlacemark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

/**
 * Renders GeoJsonFeature objects onto the GoogleMap as Marker, Polyline and Polygon objects. Also
 * removes GeoJsonFeature objects and redraws features when updated.
 */
public class GeoJsonRenderer extends Renderer implements Observer {

    private final static Object FEATURE_NOT_ON_MAP = null;

    /**
     * Creates a new GeoJsonRender object
     *
     * @param map map to place GeoJsonFeature objects on
     * @param features contains a hashmap of features and objects that will go on the map
     */
    public GeoJsonRenderer(GoogleMap map, HashMap<GeoJsonFeature, Object> features) {
        super(map, features);

    }

    /**
     * Changes the map that GeoJsonFeature objects are being drawn onto. Existing objects are
     * removed from the previous map and drawn onto the new map.
     *
     * @param map GoogleMap to place GeoJsonFeature objects on
     */
    public void setMap(GoogleMap map) {
        super.setMap(map);
        for (Feature feature : super.getFeatures()) {
            redrawFeatureToMap((GeoJsonFeature) feature, map);
        }
    }

    /**
     * Adds all of the stored features in the layer onto the map if the layer is
     * not already on the map.
     */
    public void addLayerToMap() {
        if (!isLayerOnMap()) {
            setLayerVisibility(true);
            Log.e("renderer","add layer to map");
            for (Feature feature : super.getFeatures()) {

                addFeature((GeoJsonFeature) feature);
            }
        }
    }

    public void addLayerAsync(VisibleRegion visRegion){
        if (!isLayerOnMap()) {
            setLayerVisibility(true);
            //Log.e("layer async","in addlayerasync");
            for (Feature feature : super.getFeatures()) {
                //List<LatLng> list = ((GeoJsonLineString) feature).getCoordinates();
                //Log.e("layer async","layer has features");
                List<LatLng> lis=new ArrayList<>();
                if(feature.getGeometry().getGeometryType().equals("LineString")) {
                    //Log.e("layer async","equals linestring");
                    lis.add( ((List<LatLng>) feature.getGeometry().getGeometryObject()).get(0));
                    lis.add( ((List<LatLng>) feature.getGeometry().getGeometryObject()).get(((List<LatLng>) feature.getGeometry().getGeometryObject()).size()-1));
                }else if(feature.getGeometry().getGeometryType().equals("MultiLineString")) {
                    lis.add(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().get(0));
                    lis.add(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().get(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().size()-1));;
                }else if(!((GeoJsonFeature) feature).isVisible() && feature.getGeometry().getGeometryType().equals("MultiPolygon")) {
                    List<GeoJsonPolygon> geoPoly = (List<GeoJsonPolygon>) feature.getGeometry().getGeometryObject();
                    Log.e("geoPoly", geoPoly.get(0).getGeometryType());
                }else if(!((GeoJsonFeature) feature).isVisible() && feature.getGeometry().getGeometryType().equals("Polygon")) {
                    List<List<LatLng>> geoPoly = (List<List<LatLng>>) feature.getGeometry().getGeometryObject();
                    lis=geoPoly.get(0);
                }else{
                    Log.e("Georenderer","Feature type not dealt with in async");
                }
                if(!lis.isEmpty()) {
                    int size = lis.size();
                    //If first or last point of line is in visible region add to map otherwise don't
                    if ((lis.get(0).latitude > visRegion.nearRight.latitude &&
                            lis.get(0).latitude < visRegion.farLeft.latitude &&
                            lis.get(0).longitude < visRegion.nearRight.longitude &&
                            lis.get(0).longitude > visRegion.farLeft.longitude) ||
                            (lis.get(size - 1).latitude > visRegion.nearRight.latitude &&
                                    lis.get(size - 1).latitude < visRegion.farLeft.latitude &&
                                    lis.get(size - 1).longitude < visRegion.nearRight.longitude &&
                                    lis.get(size - 1).longitude > visRegion.farLeft.longitude)) {
                        //Log.e("addlayerasync", "adding");
                        ((GeoJsonFeature) feature).setVisible(true);

                        addFeature((GeoJsonFeature) feature);
                    }
                }
            }
        }
    }


    public void updateLayerAsync(VisibleRegion visRegion){
        if (isLayerOnMap()) {
            Set<Feature> features = super.getFeatures();
            for (Feature feature : new ArrayList<Feature>(super.getFeatures())) {
            //for (Iterator<Feature> it = super.getFeatures().iterator(); it.hasNext();) {
            //List<LatLng> list = ((GeoJsonLineString) feature).getCoordinates();
                //!((GeoJsonFeature)feature).isVisible()&&
                    List<LatLng> lis = new ArrayList<>();

                    if (feature.getGeometry().getGeometryType().equals("LineString")) {
                        lis.add( ((List<LatLng>) feature.getGeometry().getGeometryObject()).get(0));
                        lis.add( ((List<LatLng>) feature.getGeometry().getGeometryObject()).get(((List<LatLng>) feature.getGeometry().getGeometryObject()).size()-1));
                    } else if (feature.getGeometry().getGeometryType().equals("MultiLineString")) {
                        lis.add(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().get(0));
                        lis.add(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().get(((List<GeoJsonLineString>) feature.getGeometry().getGeometryObject()).get(0).getCoordinates().size()-1));
                    } else if(feature.getGeometry().getGeometryType().equals("MultiPolygon")) {
                        List<GeoJsonPolygon> geoPoly = (List<GeoJsonPolygon>) ((GeoJsonMultiPolygon) feature.getGeometry().getGeometryObject()).getPolygons();
                        Log.e("geoPoly", geoPoly.get(0).toString());
                    }else if(feature.getGeometry().getGeometryType().equals("Polygon")) {
                        List<List<LatLng>> geoPoly = (List<List<LatLng>>) feature.getGeometry().getGeometryObject();
                        lis=geoPoly.get(0);
                    }else {
                        //Todo: multi Polygon
                        Log.e("Georenderer", "Feature type not dealt with in updateAsync " + feature.getGeometry().getGeometryType());
                    }
                    if (!lis.isEmpty()) {
                        int size = lis.size();
                        //If first or last point of line is in visible region add to map otherwise don't
                        if (!((GeoJsonFeature) feature).isVisible() && ((lis.get(0).latitude > visRegion.nearRight.latitude &&
                                lis.get(0).latitude < visRegion.farLeft.latitude &&
                                lis.get(0).longitude < visRegion.nearRight.longitude &&
                                lis.get(0).longitude > visRegion.farLeft.longitude) ||
                                (lis.get(size - 1).latitude > visRegion.nearRight.latitude &&
                                        lis.get(size - 1).latitude < visRegion.farLeft.latitude &&
                                        lis.get(size - 1).longitude < visRegion.nearRight.longitude &&
                                        lis.get(size - 1).longitude > visRegion.farLeft.longitude))) {
                            //Log.e("addlayerasync", "adding");
                            addFeature((GeoJsonFeature) feature);
                            ((GeoJsonFeature) feature).setVisible(true);
                        } else if (((GeoJsonFeature) feature).isVisible()&& !((lis.get(0).latitude > visRegion.nearRight.latitude &&
                                lis.get(0).latitude < visRegion.farLeft.latitude &&
                                lis.get(0).longitude < visRegion.nearRight.longitude &&
                                lis.get(0).longitude > visRegion.farLeft.longitude) &&
                                (lis.get(size - 1).latitude > visRegion.nearRight.latitude &&
                                        lis.get(size - 1).latitude < visRegion.farLeft.latitude &&
                                        lis.get(size - 1).longitude < visRegion.nearRight.longitude &&
                                        lis.get(size - 1).longitude > visRegion.farLeft.longitude))) {
                            ((GeoJsonFeature) feature).setVisible(false);
                            removeFeatureFromMap((GeoJsonFeature) feature);
                        }
                    } else {
                        Log.e("geoRenderer", "lis is empty");
                    }

            }
        }else{ Log.e("geoRenderer", "layer is not on map update");}
    }

    public void setLayerStyle(int color){

                super.setLayerStyle(color);
    }

    /**
     * Adds a new GeoJsonFeature to the map if its geometry property is not null.
     *
     * @param feature feature to add to the map
     */
   public void addFeature(GeoJsonFeature feature) {
        super.addFeature(feature);
        if (isLayerOnMap()) {
            feature.addObserver(this);
        }
    }

    /**
     * Removes all GeoJsonFeature objects stored in the mFeatures hashmap from the map
     */
    public void removeLayerFromMap() {
        if (isLayerOnMap()) {
            for (Feature feature : super.getFeatures()) {
                ((GeoJsonFeature) feature).setVisible(false);
                removeFromMap(super.getAllFeatures().get(feature));
                feature.deleteObserver(this);
            }
            setLayerVisibility(false);
        }
    }


    /**
     * Removes a GeoJsonFeature from the map if its geometry property is not null
     *
     * @param feature feature to remove from map
     */
   public void removeFeatureFromMap(GeoJsonFeature feature) {
        // Check if given feature is stored

        if (super.getFeatures().contains(feature)) {
            //super.removeFeature(feature);
            feature.deleteObserver(this);
            removeFromMap(super.getAllFeatures().get(feature));
            feature.setVisible(false);

        }
    }

    /**
     * Redraws a given GeoJsonFeature onto the map. The map object is obtained from the mFeatures
     * hashmap and it is removed and added.
     *
     * @param feature feature to redraw onto the map
     */
    private void redrawFeatureToMap(GeoJsonFeature feature) {
        redrawFeatureToMap(feature, getMap());
    }

    private void redrawFeatureToMap(GeoJsonFeature feature, GoogleMap map) {
        removeFromMap(getAllFeatures().get(feature));
        putFeatures(feature, FEATURE_NOT_ON_MAP);
        if (map != null && feature.hasGeometry()) {
            putFeatures(feature, addGeoJsonFeatureToMap(feature, feature.getGeometry()));
        }
    }

    /**
     * Update is called if the developer sets a style or geometry in a GeoJsonFeature object
     *
     * @param observable GeoJsonFeature object
     * @param data       null, no extra argument is passed through the notifyObservers method
     */
    public void update(Observable observable, Object data) {
        if (observable instanceof GeoJsonFeature) {
            GeoJsonFeature feature = ((GeoJsonFeature) observable);
            boolean featureIsOnMap = getAllFeatures().get(feature) != FEATURE_NOT_ON_MAP;
            if (featureIsOnMap && feature.hasGeometry()) {
                // Checks if the feature has been added to the map and its geometry is not null
                // TODO: change this so that we don't add and remove
                redrawFeatureToMap(feature);
            } else if (featureIsOnMap && !feature.hasGeometry()) {
                // Checks if feature is on map and geometry is null
                removeFromMap(getAllFeatures().get(feature));
                putFeatures(feature, FEATURE_NOT_ON_MAP);
            } else if (!featureIsOnMap && feature.hasGeometry()) {
                // Checks if the feature isn't on the map and geometry is not null
                addFeature(feature);
            }
        }
    }
}
